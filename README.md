# 1ª Contituição da Irmandade
Blumenau, 30 de maio de 2018.

_Esta constituição, acordada por ambas as partes, serve para regulamentar quaisquer ações, tratamentos e demais coisas entre Lucas Samuel e Matheus Boing._

`Última atualização em 17 de julho de 2018 por Lucas Samuel.`


## TÍTULO I - DA VALIDADE
_Itens determinantes da validade da constituição e seus itens._

### Art. 1º - Do acordo
**Parágrafo único -** Para a validade, alteração, inclusão ou exclusão de qualquer item desta constituição, ambas as partes deverão entrar em comum acordo causando a invalidade da tal em caso contrário. A data deverá ser atualizada sempre que a constituição for alterada.

### Art. 2º - Da publicação
**§ 1º -** Itens adicionados à constituição passam a valer à partir da data de publicação.

**§ 2º -** A publicação será válida mediante aprovação de ambas as partes e, após efetuada, não poderá ser revogada.

**§ 3º -** Será valida somente, e tão somente, a constituição publicada em vigor no repositório: (https://bitbucket.org/lukisamk/1a-contituicao-da-irmandade/src/master/).

### Art. 3º - Da alteração

**§ 1º -** Adições na constituição deverão ser efetuadas mediante autorização de ambas as partes, sofrendo revisões e completa adequação antes de ser publicada. Itens adicionados à constituição passam a valer à partir da data de publicação.

**§ 2º -** Alterações na constituição deverão ser completamente commitadas, justificando tais coisas.

**$ 3º -** Nenhum item poderá ser excluído da constituição, apenas revogado, sobrescrevendo todo o item revogado ~~desta maneira~~.

### Art. 4º - Da consciência
**Parágrafo único -** Deve-se estar claro e compreensível que, para a validade desta constituição, ambas as partes devem ter consciência de seus fatos e desejar o bom convívio na irmandade. Deixa-se exposto que a intenção da elaboração desta constituição é tão somente para questionar a ciência dos atos de cada um.

## TÍTULO II – DAS PENALIZAÇÕES
_Diretrizes para penalizações de não cumprimento da constituição._

### Art. 5º - Níveis e penalizações
**§ 1º -** Será classificada como penalidade leve aquela que for não intencional ou onde estiver determinada nos itens de cada artigo. O recorrente poderá enviar o emoji 👍 sem sofrer qualquer penalidade referente ao uso deste item, desde que autorizado pela própria constituição. Não é permitido o uso dos parágrafos 2º e 3º do artigo 5º desde que não constitucional.

**§ 2º -** Será classificada como penalidade moderada aquela que for intencional ou onde estiver determinada nos itens de cada artigo. O recorrente poderá fazer uso do parágrafo 1º do artigo 5º e manter silêncio (popular vaco) desde que a outra parte não faça uso do parágrafo 3º do artigo 5º da constituição. Não é permitido o uso do parágrafo 3º do artigo 5º desde que não constitucional.

**§ 3º -** Será classificada como penalidade gravíssima aquela que for intencional e presencial ou onde estiver determinada nos itens de cada artigo. O recorrente poderá fazer uso do parágrafo 1º do artigo 5º e manter silêncio (popular vaco) desde que a outra parte não faça uso do parágrafo 3º do artigo 6º da constituição. 

**§ 4º -** A penalidade não constitucional ou citação que não esteja na constituição será classificada como inconstitucional, quebrando a argumentação independente de qual seja. A parte que fizer uso de algo inconstitucional deverá assumir o erro e sofrer penalidade leve.

## TÍTULO III – DO PEDIDO DE DESCULPA
_Diretrizes para resolução de quebra da constituição._

### Art. 6º - Do pedido
**§ 1º -** Deverá ter a iniciativa de pedir perdão o infrator, ou seja, aquele que quebrou a constituição.

**§ 2º -** O infrator poderá solicitar a explanação do motivo pela qual sofrer a penalidade, desde que assuma a culpa e mantenha o pedido de perdão. Para tal, poderá usar a simples frase “direito do infrator”.

**§ 3º -** O infrator poderá solicitar o término da penalidade desde que tome a iniciativa na conversa e peça sinceras desculpas. O recorrente deverá conceder o perdão desde que a quebra não seja gravíssima.

**§ 4º -** No caso de quebra gravíssima, o infrator deverá ter a iniciativa na conversa, buscando resolver o problema com pacificidade e assumindo a culpa.

## TÍTULO III – DO BOM CONVÍVIO

### Art. 7º - Das correções
**Parágrafo único -** Não é aceita correção ortográfica ou gramatical para erros intencionais ou não intencionais de qualquer parte e por nenhuma parte seja qual for o motivo, sendo classificada como leve a quebra deste artigo.

### Art. 8º - Da não resposta
**Parágrafo único -** Dada a visualização da mensagem, a mesma deverá ser respondida sem demorado tempo de espera, caracterizando _vaco_ em caso contrário. A quebra deste item é classificada como moderada se for no período de até 10 minutos, sendo caracterizada como gravíssima em períodos maiores.

### Art. 9 - Da grosseria `[SOB ANÁLISE]`
**Parágrafo único -** É proibida grosseria intencional por ambas as partes, sendo classificada como moderada a quebra deste artigo.

## TÍTULO IV - DAS RESPONSABILIDADES

### Art. único `[SOB ANÁLISE]`
**Parágrafo único -** Fica estabelecida a hierarquia de relevância situacional para julgamento de tomada de decisão segundo a lista abaixo em ordem crescente:
* 1º - Sobrevivência
* 2º - Responsabilidades
* 3º - Sistemas de tecnologia da informação
* 4º - League of Legends